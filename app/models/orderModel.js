//Import thư viện mongooseJs
const mongoose = require("mongoose");

//Khai báo class Schema của thư viện mongooseJs
const Schema = mongoose.Schema;
// khai báo product schema

const orderSchema = new Schema({
    // _id có thể khai bảo hoặc không
    // _id: {
    //     type: mongoose.Types.ObjectId,
    //     required: true
    // },
    orderDate: {
        type: Date,
        default: Date.now(),
    },
    shippedDate: {
        type: Date
    },
    note: {
        type: String,
    },
    orderDetails: [{
        type: mongoose.Types.ObjectId,
        ref: "OrderDetail"
    }],
    cost: {
        type: Number,
        default: 0,
    },
    customerId: {
        type: mongoose.Types.ObjectId,
        ref: "Customer"
    },
}, {
    timestamps: true
});

module.exports = mongoose.model("Order", orderSchema);

