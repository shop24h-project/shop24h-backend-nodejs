//Import thư viện mongooseJs
const mongoose = require("mongoose");

//Khai báo class Schema của thư viện mongooseJs
const Schema = mongoose.Schema;
// khai báo product schema

const productSchema = new Schema({
    // _id có thể khai bảo hoặc không
    // _id: {
    //     type: mongoose.Types.ObjectId,
    //     required: true
    // },
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false,
    },
    type: {
        type: mongoose.Types.ObjectId,
        ref: "ProductType"
    },
    imageUrl: {
        type: String,
        required: true,
    },
    buyPrice: {
        type: Number,
        required: true,
    },
    promotionPrice: {
        type: Number,
        required: true,
    },
    amount: {
        type: Number,
        default: 0,
    },
    isPopular: {
        type: Boolean,
        default: true,
    },
    color: [
        {
            type: String,
            require: true
        }
    ],
    subImage: [ {
        type: String,
        required: true,
    }]

}, {
    timestamps: true
});

module.exports = mongoose.model("Product", productSchema);

