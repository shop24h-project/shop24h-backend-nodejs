//Import thư viện mongooseJs
const mongoose = require("mongoose");

//Khai báo class Schema của thư viện mongooseJs
const Schema = mongoose.Schema;
// khai báo drink schema

const productTypeSchema = new Schema({
    // _id có thể khai bảo hoặc không
    // _id: {
    //     type: mongoose.Types.ObjectId,
    //     required: true
    // },
    name: {
        type: String,
        required: true,
        unique: true
    },
    imageUrl: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: false,
    },
}, {
    timestamps: true
});

module.exports = mongoose.model("ProductType", productTypeSchema);

