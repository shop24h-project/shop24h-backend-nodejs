// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Drink Model
const productTypeModel = require("../models/productTypeModel");

// Create Product Type
const CreateProductType = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let body = req.body;

    // B2: Validate dữ liệu
    if (!body.name) {
        return res.status(400).json({
            message: "name is required!"
        })
    }
    if (!body.imageUrl) {
        return res.status(400).json({
            message: "imageUrl is required!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newProductTypeData = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        imageUrl: body.imageUrl,
        description: body.description,
    }

    productTypeModel.create(newProductTypeData, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(201).json({
            message: "Create successfully",
            newProductType: data
        })
    })
}

//Get all Product Type
const GetAllProductType = (req, res) => {
    const { limit, name} = req.query;

    const condition = {};
    if(name) {
        const regex = new RegExp(`${name}`)
        condition.name = regex
    }



    // B1: Thu thập dữ liệu từ req
    // B2: Validate dữ liệu
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    productTypeModel.find(condition).limit(limit).exec((error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Get all product type successfully",
            productType: data
        })
    })
}

//Get Product Type by id
const GetProductTypeByID = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let productTypeId = req.params.productTypeId;
    console.log("productTypeId", productTypeId);
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            message: "Product Type ID is invalid!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    productTypeModel.findById(productTypeId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(201).json({
            message: "Get Product Type successfully",
            productType: data
        })
    })
}

//Update Product Type
const UpdateProductType = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let productTypeId = req.params.productTypeId;
    let body = req.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            message: "Product Type ID is invalid!"
        })
    }

    // Bóc tách trường hợp undefied
    if (body.name !== undefined && body.name == "") {
        return res.status(400).json({
            message: "name is required!"
        })
    }
    if (body.imageUrl !== undefined && body.imageUrl == "") {
        return res.status(400).json({
            message: "imageUrl is required!"
        })
    }



    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let updateProductTypeData = {
        name: body.name,
        description: body.description,
        imageUrl: body.imageUrl
    }

    productTypeModel.findByIdAndUpdate(productTypeId, updateProductTypeData, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Update product Type successfully",
            updatedProductType: data
        })
    })
}

// Delete Product Type
const DeleteProductType = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let productTypeId = req.params.productTypeId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            message: "Product Type ID is invalid!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    productTypeModel.findByIdAndDelete(productTypeId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(204).json({
            message: "Delete Product Type successfully"
        })
    })
}

// Export Drink controller thành 1 module
module.exports = {
    GetAllProductType,
    GetProductTypeByID,
    CreateProductType,
    UpdateProductType,
    DeleteProductType
}


