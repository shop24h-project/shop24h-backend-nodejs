// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Order Model
const orderModel = require("../models/orderModel");
const orderDetailModel = require("../models/orderDetailModel");
const customerModel = require("../models/customerModel");


// create Order Of Customer
const createOrder = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    // let customerId = req.params.customerId;
    let body = req.body;
    let orderDetail = body.orderDetail;
    // return res.status(200).json({
    //     message: "test"
    // })
    // console.log(customerId);
    //B2: Validate dữ liệu
    // if(!mongoose.Types.ObjectId.isValid(customerId)) {
    //     return res.status(400).json({
    //         status: "Error 400: Bad req",
    //         message: "Customer ID is invalid"
    //     })
    // }
    if (!body.phone) {
        return res.status(400).json({
            message: "Phone is required"
        })
    }
    // if(!body.orderDate) {
    //     return res.status(400).json({
    //         message: "orderDate is invalid"
    //     })
    // }
    // if(!body.shippedDate) {
    //     return res.status(400).json({
    //         message: "shipped Date is invalid"
    //     })
    // }

    // if(!Number.isInteger(body.cost) || body.cost < 0) {
    //     return res.status(400).json({
    //         message: "Cost is invalid!"
    //     })
    // }

    customerModel.findOne({
        phone: body.phone
    }, (errorFindCustomer, customerExist) => {
        if (errorFindCustomer) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: errorFindCustomer.message
            })
        }
        else {
            if (!customerExist) {
                customerModel.create({
                    _id: mongoose.Types.ObjectId(),
                    fullName: body.fullName,
                    phone: body.phone,
                    email: body.email,
                    address: body.address,
                    city: body.city,
                    country: body.country
                }, (errCreateCustomer, CustomerCreated) => {
                    if (errCreateCustomer) {
                        return res.status(500).json({
                            status: "Error 500: Internal server error",
                            message: errCreateCustomer.message
                        })
                    }
                    else {
                        orderModel.create({
                            _id: mongoose.Types.ObjectId(),
                            orderDate: body.shippedDate,
                            shippedDate: body.shippedDate,
                            note: body.note,
                            cost: body.cost,
                            city: body.city,
                        }, (errCreateOrder, OrderCreated) => {
                            if (errCreateOrder) {
                                return res.status(500).json({
                                    status: "Error 500: Internal server error",
                                    message: errCreateCustomer.message
                                })
                            }
                            else {
                                customerModel.findByIdAndUpdate(CustomerCreated._id,
                                    {
                                        $push: { orders: OrderCreated._id }
                                    },
                                    (err, updatedcustomer) => {
                                        if (err) {
                                            return res.status(500).json({
                                                status: "Error 500: Internal server error",
                                                message: err.message
                                            })
                                        } else {
                                            var errorDetail = null;
                                            var dataDetail = null;
                                            for (var bI = 0; bI < orderDetail.length; bI++) {
                                                const orderDetailObject = {
                                                    _id: mongoose.Types.ObjectId(),
                                                    product: orderDetail[bI]._id,
                                                    quantity: orderDetail[bI].count,
                                                };
                                                orderDetailModel.create(orderDetailObject, (error, data) => {

                                                    if (error) {
                                                        errorDetail = error;
                                                    } else {
                                                        orderModel.findByIdAndUpdate(OrderCreated._id,
                                                            {
                                                                $push: { orderDetails: data._id }
                                                            },
                                                            (err, updatedOrder) => {
                                                                if (err) {
                                                                    errorDetail = err;
                                                                }
                                                                else {
                                                                    dataDetail = updatedOrder;
                                                                }
                                                            }
                                                        )
                                                    }
                                                })
                                            }
                                            if (errorDetail) {
                                                return res.status(500).json({
                                                    status: "Error 500: Internal server error",
                                                    message: err.message
                                                })
                                            }
                                            else {
                                                return res.status(201).json({
                                                    status: "Create Order Success",
                                                    data: dataDetail
                                                })
                                            }

                                        }
                                    }
                                )

                            }
                        })
                    }
                })
            }
            else {

                customerModel.findByIdAndUpdate(customerExist._id, {
                    fullName: body.fullName,
                    phone: body.phone,
                    email: body.email,
                    address: body.address,
                    city: body.city,
                    country: body.country
                }, (errorCustomerUpdate, CustomerUpdated) => {
                    if (errorCustomerUpdate) {
                        return res.status(500).json({
                            message: error.message
                        })
                    }
                    else {
                        orderModel.create({
                            _id: mongoose.Types.ObjectId(),
                            orderDate: body.shippedDate,
                            shippedDate: body.shippedDate,
                            note: body.note,
                            cost: body.cost,
                            city: body.city,
                        }, (errCreateOrder, OrderCreated) => {
                            if (errCreateOrder) {
                                return res.status(500).json({
                                    status: "Error 500: Internal server error",
                                    message: errCreateCustomer.message
                                })
                            }
                            else {
                                customerModel.findByIdAndUpdate(customerExist._id,
                                    {
                                        $push: { orders: OrderCreated._id }
                                    },
                                    (err, updatedcustomer) => {
                                        if (err) {
                                            return res.status(500).json({
                                                status: "Error 500: Internal server error",
                                                message: err.message
                                            })
                                        } else {
                                            var errorDetail = null;
                                            const dataDetail = [];
                                            for (var bI = 0; bI < orderDetail.length; bI++) {
                                                const orderDetailObject = {
                                                    _id: mongoose.Types.ObjectId(),
                                                    product: orderDetail[bI]._id,
                                                    quantity: orderDetail[bI].count,
                                                };
                                                orderDetailModel.create(orderDetailObject, (error, data) => {
                                                    if (error) {
                                                        errorDetail = error;
                                                    } else {
                                                        orderModel.findByIdAndUpdate(OrderCreated._id,
                                                            {
                                                                $push: { orderDetails: data._id }
                                                            },
                                                            (err, updatedOrder) => {
                                                                if (err) {
                                                                    errorDetail = err;
                                                                }
                                                                else {
                                                                    dataDetail.push(updatedOrder);

                                                                }
                                                            }
                                                        )
                                                    }
                                                })
                                            }
                                            console.log(dataDetail)
                                            if (errorDetail) {
                                                return res.status(500).json({
                                                    status: "Error 500: Internal server error",
                                                    message: err.message
                                                })
                                            }
                                            else {

                                                return res.status(201).json({
                                                    status: "Create Order Success",
                                                    data: ""
                                                })
                                            }
                                        }
                                    }
                                )
                            }
                        })
                    }
                })
            }
        }
    })



    // orderModel.create(newOrderInput, (error, data) => {
    //     if(error) {
    //         return res.status(500).json({
    //             status: "Error 500: Internal server error",
    //             message: error.message
    //         })
    //     } else {
    //         customerModel.findByIdAndUpdate(customerId, 
    //             {
    //                $push: { orders: data._id } 
    //             },
    //             (err, updatedcustomer) => {
    //                 if(err) {
    //                     return ResizeObserver.status(500).json({
    //                         status: "Error 500: Internal server error",
    //                         message: err.message
    //                     })
    //                 } else {
    //                     return res.status(201).json({
    //                         status: "Create Order Success",
    //                         data: data
    //                     })
    //                 }
    //             }
    //         )
    //     }
    // })
}
const createOrderOfCustomer = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    // let customerId = req.params.customerId;
    let body = req.body;
    let customerId = req.params.customerId;
    console.log(parseInt(body.cost));
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "Customer ID is invalid"
        })
    }

    if(!Number.isInteger(parseInt(body.cost)) || parseInt(body.cost) < 0) {
        return res.status(400).json({
            message: "Cost is invalid!"
        })
    }
            orderModel.create({
                _id: mongoose.Types.ObjectId(),
                orderDate: body.shippedDate,
                shippedDate: body.shippedDate,
                note: body.note,
                cost: parseInt(body.cost),
                customerId: body.customerId
            }, (errCreateOrder, OrderCreated) => {
                if (errCreateOrder) {
                    return res.status(500).json({
                        status: "Error 500: Internal server error",
                        message: errCreateOrder.message
                    })
                }
                else {
                    customerModel.findByIdAndUpdate(customerId,
                        {
                            $push: { orders: OrderCreated._id }
                        },
                        (err, data) => {
                            if (err) {
                                return res.status(500).json({
                                    status: "Error 500: Internal server error",
                                    message: err.message
                                })
                            } else {
                                return res.status(200).json({
                                    status: "Success: create order success",
                                    data: data
                                })
                            }
                        }
                    )

                }
            })
       

}

// Get all Order 
const getAllOrder = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    orderModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Get all order success",
                data: data
            })
        }
    })
}

const getAllOrderOfCustomer = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let customerId = request.params.customerId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Customer ID is invalid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    customerModel.findById(customerId)
        .populate("orders")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get data success",
                    data: data.orders
                })
            }
        })
}

const getOrderById = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    let orderId = req.params.orderId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "OrderId ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Get order success",
                data: data
            })
        }
    })
}

const updateOrderById = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    let orderId = req.params.orderId;
    let body = req.body;

    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "Order ID is not valid"
        })
    }
    if (body.orderDate !== undefined && body.orderDate == "") {
        return res.status(400).json({
            message: "orderDate is invalid!"
        })
    }
    if (body.shippedDate !== undefined && body.shippedDate == "") {
        return res.status(400).json({
            message: "shippedDate is invalid!"
        })
    }
    if (body.cost !== undefined && (!Number.isInteger(parseInt(body.cost)) || parseInt(body.cost) <= 0)) {
        return res.status(400).json({
            message: "cost is invalid!"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let OrderUpdate = {
        orderDate: body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        cost: body.cost,
    }


    orderModel.findByIdAndUpdate(orderId, OrderUpdate, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Update order success",
                data: data
            })
        }
    })
}

const deleteOrderById = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    let customerId = req.params.customerId;
    let orderId = req.params.orderId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "customer ID is not valid"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "Order ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    orderModel.findByIdAndDelete(orderId, (error) => {
        if (error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            // Sau khi xóa xong 1 order khỏi collection cần xóa thêm orderID trong customer đang chứa nó
            customerModel.findByIdAndUpdate(customerId,
                {
                    $pull: { orders: orderId }
                },
                (err, updatedcustomer) => {
                    if (err) {
                        return res.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        return res.status(204).json({
                            status: "Success: Delete order success"
                        })
                    }
                })
        }
    })
}


// Export customer controller thành 1 module
module.exports = {
    createOrder,
    createOrderOfCustomer,
    getAllOrder,
    getAllOrderOfCustomer,
    getOrderById,
    updateOrderById,
    deleteOrderById
}