// Import thư viện mongoose
const mongoose = require("mongoose");

// Import product Model
const productModel = require("../models/productModel");

// Create Product 
const createProduct  = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let body = req.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(body.type)) {
        return res.status(400).json({
            message: "Product Type ID is invalid!"
        })
    }
    if (!body.name) {
        return res.status(400).json({
            message: "name is required!"
        })
    }
    if (!body.imageUrl) {
        return res.status(400).json({
            message: "imageUrl is required!"
        })
    }
    if (!body.subImage) {
        return res.status(400).json({
            message: `subImage is required`
        })
    }
    if (!body.color) {
        return res.status(400).json({
            message: `color is required`
        })
    }
    
    if(!Number.isInteger(parseInt(body.buyPrice)) || parseInt(body.buyPrice) <= 0) {
        return res.status(400).json({
            message: "buy Price is invalid!"
        })
    }
    if(!Number.isInteger(parseInt(body.promotionPrice)) || parseInt(body.promotionPrice) <= 0) {
        return res.status(400).json({
            message: "Promotion Price is invalid!"
        })
    }
    if(!Number.isInteger(parseInt(body.amount)) || parseInt(body.amount) < 0) {
        return res.status(400).json({
            message: "Amount is invalid!"
        })
    }


    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newProductData = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
        type: body.type,
        imageUrl: body.imageUrl,
        color: body.color,
        subImage: body.subImage,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount,
    }

    productModel.create(newProductData, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(201).json({
            
            message: "Create successfully",
            newProduct: data
        })
    })
}

//Get all Product Type
const getAllProduct= (req, res) => {
    const {isPopular, limit, priceMin, priceMax, name, categoryId} = req.query;
    const condition = {};
    console.log("categoryId", categoryId);
    if(categoryId) {
        condition.type = categoryId
    }
    if(name) {
        const regex = new RegExp(`${name}`)
        condition.name = regex
    }
    if(isPopular) {
        condition.isPopular = isPopular;
    }
    if(priceMin) {
        condition.promotionPrice = {
            ...condition.noStudent,
            $gte: priceMin
        }
    }

    if(priceMax) {
        condition.promotionPrice = {
            ...condition.promotionPrice,
            $lte: priceMax
        }
    }


    // B1: Thu thập dữ liệu từ req
    // B2: Validate dữ liệu
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    productModel.find(condition).limit(limit).exec((error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Get all product successfully",
            product: data
        })
    })
}

//Get Product by id
const getProductById = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let productId = req.params.productId;
    console.log("productId", productId);
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            message: "Product ID is invalid!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    productModel.findById(productId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(201).json({
            message: "Get Product successfully",
            product: data
        })
    })
}

//Update Product 
const updateProduct  = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let productId = req.params.productId;
    let body = req.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            message: "Product Type ID is invalid!"
        })
    }

    // Bóc tách trường hợp undefied
    if (body.name !== undefined && body.name == "") {
        return res.status(400).json({
            message: "name is required!"
        })
    }
    if (body.imageUrl !== undefined && body.imageUrl == "") {
        return res.status(400).json({
            message: "imageUrl is required!"
        })
    }
    if (body.subImage !== undefined && body.subImage == "") {
        return res.status(400).json({
            message: "subImage is required!"
        })
    }
    if (body.color !== undefined && body.color == "") {
        return res.status(400).json({
            message: `color is required`
        })
    }
    
    
    if(body.buyPrice !== undefined && (!Number.isInteger(body.buyPrice) || body.buyPrice <= 0)) {
        return res.status(400).json({
            message: "buyPrice is invalid!"
        })
    }
    if(body.promotionPrice !== undefined && (!Number.isInteger(body.promotionPrice) || body.promotionPrice <= 0)) {
        return res.status(400).json({
            message: "promotionPrice is invalid!"
        })
    }
    if(body.amount !== undefined && (!Number.isInteger(body.amount) || body.amount <= 0)) {
        return res.status(400).json({
            message: "amount is invalid!"
        })
    }



    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let updateProductData = {
        name: body.name,
        description: body.description,
        imageUrl: body.imageUrl,
        color: body.color,
        imageChild: body.imageChild,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        subImage: body.subImage,
        amount: body.amount,
    }

    productModel.findByIdAndUpdate(productId, updateProductData, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Update product successfully",
            updatedProduct: data
        })
    })
}

// Delete Product Type
const deleteProduct= (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let productId = req.params.productId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            message: "Product ID is invalid!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    productModel.findByIdAndDelete(productId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(204).json({
            message: "Delete Product successfully"
        })
    })
}

// Export Drink controller thành 1 module
module.exports = {
    createProduct,
    getAllProduct,
    getProductById,
    updateProduct,
    deleteProduct 
}


