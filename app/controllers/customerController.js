// Import thư viện mongoose
const mongoose = require("mongoose");

// Import CustomerModel
const customerModel = require("../models/customerModel");
function validateEmail(mail) 
{
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {
    return true;
  }
    return false;
}
// Create Customer
const createCustomer = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let body = req.body;

    // B2: Validate dữ liệu
    if (!body.fullName) {
        return res.status(400).json({
            message: "fullName is required!"
        })
    }
    if (!body.phone) {
        return res.status(400).json({
            message: "phone is required!"
        })
    }
    if(!validateEmail(body.email)) {
        return res.status(400).json({
          message: "email is invalid!"
        })
    }



    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newCustomerData = {
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country,
    }

    customerModel.create(newCustomerData, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(201).json({
            message: "Create successfully",
            newCustomer: data
        })
    })
}

//Get all Customer
const getAllCustomer= (req, res) => {
    // B1: Thu thập dữ liệu từ req
    // B2: Validate dữ liệu
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    const {fullName} = req.query;

    const condition = {};
    if(fullName) {
        const regex = new RegExp(`${fullName}`)
        condition.fullName = regex
    }
    customerModel.find(condition).exec((error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Get all Customers uccessfully",
            Customer: data
        })
    })
}

//Get Customer by id
const getCustomerById = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let customerId = req.params.customerId;
    console.log("CustomerId", customerId);
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "Customer ID is invalid!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    customerModel.findById(customerId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(201).json({
            message: "Get Customer successfully",
            Customer: data
        })
    })
}

//Update Customer
const updateCustomer = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let customerId = req.params.customerId;
    let body = req.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "Customer ID is invalid!"
        })
    }

    // Bóc tách trường hợp undefied
    if (body.fullName !== undefined && body.fullName == "") {
        return res.status(400).json({
            message: "fullName is required!"
        })
    }
    if (body.phone !== undefined && body.phone == "") {
        return res.status(400).json({
            message: "phone is required!"
        })
    }
    
    if(!validateEmail(body.email)) {
        return res.status(400).json({
          message: "email is invalid!"
        })
    }
   



    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let updateCustomerData = {
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country,
    }

    customerModel.findByIdAndUpdate(customerId, updateCustomerData, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Update Customer successfully",
            updatedCustomer: data
        })
    })
}

// Delete CustomerType
const deleteCustomer= (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let customerId = req.params.customerId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "CustomerID is invalid!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    customerModel.findByIdAndDelete(customerId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(204).json({
            message: "Delete Customer successfully"
        })
    })
}
const getCustomerByEmailOrPhone = async (req, res) => {
    try {
      const key = req.query.key;
      const customer = await customerModel.findOne({
        $or: [{ email: String(key) }, { phone: String(key) }],
      });
      console.log(key);
      res.status(200).json({
        success: true,
        message: "find exists",
        customer: customer,
      });
    } catch (err) {
      res.status(500).json({
        success: false,
        message: "search failed",
        error: err.message,
      });
    }
  };

// Export Drink controller thành 1 module
module.exports = {
    createCustomer,
     getAllCustomer , 
    getCustomerById , 
    updateCustomer , 
    deleteCustomer ,
    getCustomerByEmailOrPhone
}


