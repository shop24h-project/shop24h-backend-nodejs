// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Order Model
const orderDetailModel = require("../models/orderDetailModel");
const orderModel = require("../models/orderModel");


// create Order Detail Of Order
const createOrderDetailOfOrder = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    let orderId = req.params.orderId;

    let body = req.body;
    console.log(orderId);
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "OrderId ID is invalid"
        })
    }

    if(!mongoose.Types.ObjectId.isValid(body.product)) {
        return res.status(400).json({
            message: "product Id is invalid"
        })
    }
    if(!Number.isInteger(parseInt(body.quantity)) || parseInt(body.quantity) < 0) {
        return res.status(400).json({
            message: "quantity is invalid!"
        })
    }
    
  

    //B3: Thao tác với cơ sở dữ liệu
    let newOrderDetailInput = {
        _id: mongoose.Types.ObjectId(),
        product: body.product,
        quantity: body.quantity,
    }

    orderDetailModel.create(newOrderDetailInput, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            orderModel.findByIdAndUpdate(orderId, 
                {
                   $push: { orderDetails: data._id } 
                },
                (err, updatedOrder) => {
                    if(err) {
                      
                        return ResizeObserver.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        console.log(data)
                        return res.status(201).json({
                            status: "Create Order Detail Success",
                            data: data
                        })
                    }
                }
            )
        }
    })
}


const getAllOrderDetailOfOrder = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let orderId = request.params.orderId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "OrderId ID is invalid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    orderModel.findById(orderId)
        .populate("orderDetails")
        .exec((error, data) => {
            if(error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get data success",
                    data: data.orderDetails
                })
            }
        })
}

const getOrderDetailById = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    let orderDetailId = req.params.orderDetailId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "order Detail Id is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    orderDetailModel.findById(orderDetailId, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Get order detail success",
                data: data
            })
        }
    })
}

const updateOrderDetail = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    let orderDetailId = req.params.orderDetailId;
    let body = req.body;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "Order Detail ID is not valid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(body.product)) {
        return res.status(400).json({
            message: "product Id is invalid"
        })
    }
    if(body.quantity !== undefined && (!Number.isInteger(parseInt(body.quantity)) || parseInt(body.quantity) <= 0)) {
        return res.status(400).json({
            message: "quantity is invalid!"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let OrderDetailUpdate = {
        product: body.product,
        quantity: body.quantity,
    }


    orderDetailModel.findByIdAndUpdate(orderDetailId, OrderDetailUpdate, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Update order detail success",
                data: data
            })
        }
    })
}

const deleteOrderDetail = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    let orderDetailId = req.params.orderDetailId;
    let orderId = req.params.orderId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "order Detail ID is not valid"
        })
    }

    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "Order ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    orderDetailModel.findByIdAndDelete(orderDetailId, (error) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            // Sau khi xóa xong 1 order detail khỏi collection cần xóa thêm order detail ID trong order đang chứa nó
            orderModel.findByIdAndUpdate(orderId, 
                {
                    $pull: { orderDetails: orderDetailId } 
                },
                (err, updatedOrder) => {
                    if(err) {
                        return res.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        return res.status(204).json({
                            status: "Success: Delete order detail success"
                        })
                    }
                })
        }
    })
}
  // Export customer controller thành 1 module
module.exports = {
    createOrderDetailOfOrder ,
    getAllOrderDetailOfOrder ,
    getOrderDetailById ,
    updateOrderDetail ,
    deleteOrderDetail ,
}
