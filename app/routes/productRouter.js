const express = require("express");

const router = express.Router();
const productMiddleware = require('../middlewares/productMiddleware');
const authMiddleware = require('../middlewares/auth-middleware');
const { 
    createProduct, 
    getAllProduct, 
    getProductById, 
    updateProduct, 
    deleteProduct 
} = require("../controllers/productController");


//sủ dụng middle ware
router.use(productMiddleware);


router.post("/products", createProduct);

router.get("/products", getAllProduct);

router.get("/products/:productId", getProductById);

router.put("/products/:productId", updateProduct );

router.delete("/products/:productId",deleteProduct);

module.exports = router;
