const express = require("express");

const router = express.Router();

const { 
    createOrder ,
    createOrderOfCustomer,
    getAllOrder,
    getAllOrderOfCustomer,
    getOrderById,
    updateOrderById,
    deleteOrderById
} = require("../controllers/orderController");

router.post("/orders", createOrder );
router.post("/customers/:customerId/orders", createOrderOfCustomer );
router.get("/orders", getAllOrder);
router.get("/customers/:customerId/orders", getAllOrderOfCustomer );
router.get("/orders/:orderId", getOrderById);
router.put("/orders/:orderId", updateOrderById);
router.delete("/customers/:customerId/orders/:orderId", deleteOrderById);
module.exports = router;
