const express = require("express");

const router = express.Router();

const { 
    GetAllProductType, 
    GetProductTypeByID, 
    CreateProductType, 
    UpdateProductType, 
    DeleteProductType 
} = require("../controllers/productTypeController");

router.get("/product-types", GetAllProductType );

router.get("/product-types/:productTypeId", GetProductTypeByID);

router.post("/product-types", CreateProductType );

router.put("/product-types/:productTypeId", UpdateProductType )

router.delete("/product-types/:productTypeId",DeleteProductType)

module.exports = router;
