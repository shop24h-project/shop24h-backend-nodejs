const express = require("express");

const router = express.Router();

const { 
    createOrderDetailOfOrder ,
    getAllOrderDetailOfOrder ,
    getOrderDetailById ,
    updateOrderDetail ,
    deleteOrderDetail ,
} = require("../controllers/orderDetailController");

router.post("/orders/:orderId/order-details", createOrderDetailOfOrder );
router.get("/orders/:orderId/order-details", getAllOrderDetailOfOrder  );
router.get("/order-details/:orderDetailId", getOrderDetailById);
router.put("/order-details/:orderDetailId", updateOrderDetail);
router.delete("/orders/:orderId/order-details/:orderDetailId", deleteOrderDetail);
module.exports = router;
