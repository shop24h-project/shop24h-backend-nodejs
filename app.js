// Import thư viện express.js vào. Dạng Import express from "express";
const express = require("express");
var cors = require('cors')
// Import thư viện mongoosejs
const mongoose = require("mongoose");
// Khởi tạo app express
const app = express();
app.use(cors());
// Khai báo sẵn 1 port trên hệ thống
const port = 8000;

// Import router Module
const productTypeRouter = require("./app/routes/productTypeRouter");
const productRouter = require("./app/routes/productRouter");
const customerRouter = require("./app/routes/customerRouter");
const orderRouter = require("./app/routes/orderRouter");
const orderDetailRouter = require("./app/routes/orderDetailRouter");

app.use(express.json());
mongoose.connect("mongodb://localhost:27017/CRUD_Shop24h", (error) => {
    if (error) throw error;

    console.log("Connnect successfully!")
});
// app.use((req, res, next) => {
//     let today = new Date();

//     console.log("Current: ", today);

//     next();
// })

// app.use((req, res, next) => {

//     next();

//     console.log("Method: ", req.method);

//     next();
// })



// Khai báo API
app.get("/", (req, res) => {
    let today = new Date();

    // Response trả 1 chuỗi JSON có status 200
    res.status(200).json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})
app.use(productTypeRouter);
app.use(productRouter);
app.use(customerRouter);
app.use(orderRouter);
app.use(orderDetailRouter);
// Callback function
// + Nó là 1 function
// + Nó là 1 tham số của 1 hàm khác
// + Nó sẽ chạy sau khi hàm chủ thể được thực hiện
// Lắng nghe app trên cổng được khai báo 8000
app.listen(port, () => {
    console.log("App listening on port: ", port);
});
